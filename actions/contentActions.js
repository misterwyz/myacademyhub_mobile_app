import {CONTENT_CATEGORY} from "../types";
import {httpClientGet} from "../services/api";

export const categoryList = () => async dispatch => {
    httpClientGet("content/category/list")
        .then(res=>{
            // console.log(res)
            return dispatch({
                type: CONTENT_CATEGORY,
                payload: res.data
            })
        }).catch(e=>{
        return dispatch({
            type: CONTENT_CATEGORY,
            payload: undefined

        })
    })
}