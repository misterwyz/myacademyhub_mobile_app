import {REFLECTIONS,REFLECTION_FOR_TODAY} from "../types";
import {httpClientGet} from "../services/api";

export const list = () => async dispatch => {

    httpClientGet("reflection/list")
        .then(res=>{

        return dispatch({
            type: REFLECTIONS,
            payload: res.data
        })
    }).catch(e=>{
        return dispatch({
            type: REFLECTIONS,
            payload: undefined

        })
    })
}

export const today = () => async dispatch => {
    httpClientGet("reflection/today")
        .then(res=>{

            return dispatch({
                type: REFLECTION_FOR_TODAY,
                payload: res.data
            })
        }).catch(e=>{
        return dispatch({
            type: REFLECTION_FOR_TODAY,
            payload: {}

        })
    })
}