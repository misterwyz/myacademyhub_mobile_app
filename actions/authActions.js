import {PROCESS_LOGIN, SET_TOKEN, USER,LOGOUT} from '../types';
import {AsyncStorage} from 'react-native';

export const setUser = user => async dispatch => {
        return dispatch({
            type: USER,
            payload: user
        })
}



export const setToken = (token,exp) => async dispatch => {
        const now = new Date();
        const expdate = now.getTime() + exp * 1000;
        await AsyncStorage.setItem("token", token);
        await AsyncStorage.setItem("token_exp", expdate.toString());

    return dispatch({
        type: SET_TOKEN,
        payload : {token:token,expires_in:exp}
    })

}

export const processLogin = data => async dispatch => {
    return dispatch({
        type: PROCESS_LOGIN,
        payload: data

    })
}

export const authGetToken =  () =>{

    return (dispatch, getState) =>{
        return new Promise( (resolve, reject)=>{
            const token = getState().auth.token;
            // const exp = getState().auth.expires_in;
            if(!token){
                AsyncStorage.getItem("token")
                    .catch( ()=>reject("Please login"))
                    .then(tokenFromStorage =>{
                        if(!tokenFromStorage){
                            reject("Proceed to login");
                            return false
                        }

                        AsyncStorage.getItem("token_exp")
                            .then(expdate=>{


                                if(!expdate){
                                reject("Expired token");
                                return false
                            }
                        const parsedDate = new Date(expdate * 1000);
                        const now = new Date();

                        if (parsedDate > now){
                            setToken(tokenFromStorage);
                            resolve(tokenFromStorage)
                        }else{
                            reject("Expired Access");
                        }
                        })
                    })
            }else{
                const auth = getState();
                const now = new Date();
                const expiry = new Date(auth.auth.expires_in * 1000);

                if(expiry > now){
                    setToken(token);
                    resolve(token);
                }else{
                    logout()
                    reject("Expired Access");
                }

            }
        })
    }

}


export const logout =  () =>{

    return async (dispatch, getState) => {
        await AsyncStorage.removeItem("token");
        await AsyncStorage.removeItem("token_exp");
        dispatch(
            {
                type: LOGOUT,
                payload: undefined
            }
        )
    }
}