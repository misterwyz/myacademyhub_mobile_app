import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import {AsyncStorage} from 'react-native';
import rootReducer from './reducers/index';
import thunk from 'redux-thunk';

const persistConfig = {
    key: 'root',
    storage:AsyncStorage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

// const store = createStore(persistedReducer,applyMiddleware(thunk));


// export default store;

export default () => {
    let store = createStore(persistedReducer,applyMiddleware(thunk));
    let persistor = persistStore(store)
    return { store, persistor }
}