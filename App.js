import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View,Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Provider } from 'react-redux';
import store from "./store";
import AppNavigator from './navigation/AppNavigator';
import { PersistGate } from 'redux-persist/integration/react'
// export default function App(props) {
//   const [isLoadingComplete, setLoadingComplete] = useState(false);
//
//   if (!isLoadingComplete && !props.skipLoadingScreen) {
//     return (
//       <AppLoading
//         startAsync={loadResourcesAsync}
//         onError={handleLoadingError}
//         onFinish={() => handleFinishLoading(setLoadingComplete)}
//       />
//     );
//   } else {
//     return (
//       <View style={styles.container}>
//         {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
//         <AppNavigator />
//       </View>
//     );
//   }
// }



export default class App extends React.Component{
    constructor(props){
        super(props );
        this.state={
            isReady: false
        }
    }

    async loadResourcesAsync() {
        await Promise.all([
            Asset.loadAsync([
                require('./assets/images/hub_bg.png'),
                require('./assets/images/hub_logo.png')
            ]),
            Font.loadAsync({
                'gilroy-bold': require('./assets/fonts/Gilroy-ExtraBold.otf'),
                'gilroy-light': require('./assets/fonts/Gilroy-Light.otf'),
            })
        ]);
    }

    handleLoadingError(error) {
        // In this case, you might want to report the error to your error reporting
        // service, for example Sentry
        console.warn(error);
    }


    render() {

        if (!this.state.isReady) {
            return (
                <AppLoading
                    startAsync={this.loadResourcesAsync}
                    onError={this.handleLoadingError}
                    onFinish={() => this.setState({isReady:true})}
                />
            );
        }else{
            return (
                <Provider store={ store().store }>
                    <PersistGate loading={null} persistor={store().persistor}>
                        <View style={styles.container}>
                            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                            <AppNavigator />
                        </View>
                    </PersistGate>
                </Provider>
            )
        }
    }
}


function cacheImages(images) {
    return images.map(image => {
        if (typeof image === 'string') {
            return Image.prefetch(image);
        } else {
            return Asset.fromModule(image).downloadAsync();
        }
    });
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
