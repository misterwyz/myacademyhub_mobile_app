import axios from 'axios';
import {AsyncStorage} from 'react-native';
const BASE_URL = "https://lshub.herokuapp.com/api/v1/";


// AsyncStorage.getAllKeys( (err, keys)=> console.log(err, keys))

const httpClient = axios.create({
    baseURL: BASE_URL,
    timeout: 1000,
});



export const httpClientNoAuth = (endpoint,payload)=>{

    return httpClient.post(endpoint, payload)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                // console.log(error.response.data);
                // console.log(error.response.status);
                return {
                    status: 'fail',
                    message:error.response.data.message,
                    data:error.response.data.data
                }
                // console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
                throw new Error("Network timeout. Try again.")
            } else {
                // Something happened in setting up the request that triggered an Error
                throw new Error(error.message)
                console.log('Error', error.message);
            }
            // console.log(error.config);
        });
}

export const httpClientGet = async (endpoint)=>{
    let AUTH_TOKEN = null;
    await AsyncStorage.getItem("token").then(token=>
        AUTH_TOKEN = token
    )
    // console.log(AUTH_TOKEN)

    // httpClient.defaults.headers.common['Authorization'] = `Bearer ${AUTH_TOKEN}`;
    return httpClient.get(endpoint, { headers: { Authorization: `Bearer ${AUTH_TOKEN}` } })
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            // console.log(error)
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                // console.log(error.response.data);
                // console.log(error.response.status);
                return {
                    status: 'fail',
                    message:error.response.data.message,
                    data:error.response.data.data
                }
                // console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
                throw new Error("Network timeout. Try again.")
            } else {
                // Something happened in setting up the request that triggered an Error
                throw new Error(error.message)
                console.log('Error', error.message);
            }
            // console.log(error.config);
        });
}