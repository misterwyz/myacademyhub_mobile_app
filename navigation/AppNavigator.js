import React from 'react';
import { createAppContainer, createSwitchNavigator,createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import LoginScreen from "../screens/Auth/Login";
import RegisterScreen from "../screens/Auth/Register";
import InitialScreen from  "../screens/index"

// const AppStack = createStackNavigator({ Home: HomeScreen, Other: OtherScreen });
const AuthStack = createStackNavigator(
    {
        SignIn: LoginScreen,
        Register:RegisterScreen
    },
    {
    initialRouteName: 'SignIn',
    defaultNavigationOptions: {
        header:null
    }
});

export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    InitialScreen: InitialScreen,
    Main: MainTabNavigator,
    Auth: AuthStack,
  },
{
    initialRouteName: 'InitialScreen',
    }
  )
);
