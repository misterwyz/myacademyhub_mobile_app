import React from 'react';
import { Platform ,Text} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import globalStyles from '../assets/globalStyles';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import ReflectionScreen from "../screens/Media/ReflectionScreen";
import ReflectionsScreen from "../screens/Reflections/ReflectionsScreen"
import {AntDesign, Octicons, Ionicons} from '@expo/vector-icons';
import ContentScreen from "../screens/Content/ContentScreen";
import CategoriesScreen from "../screens/Content/Categories";
import TodoScreen from "../screens/Todo/TodoScreen";
import TodoListScreen from "../screens/Todo/TodoListScreen";
import DiaryScreen from "../screens/Diary/DiaryScreen";
import DiaryListScreen from "../screens/Diary/DiaryListScreen";
import SettingsScreen from "../screens/Settings/index";
import Colors from "../constants/Colors";

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {
      navigationOptions: {
          header: {
              visible: false
          }
      }
  },
});

const HomeStack = createStackNavigator(
  {
    Home: {screen:HomeScreen},
      Reflection: {
        screen:ReflectionScreen,
        headerMode:'none',
        navigationOptions: (state) => (
            {
                headerTitle: <Text style={{...globalStyles.h3}}>Reflection</Text>,
                headerShown: false,
            }
        )
    },
      Reflections: {
          screen:ReflectionsScreen,
          headerMode:'none',
          navigationOptions: (state) => (
              {
                  headerTitle: <Text style={{...globalStyles.h3}}>Reflections</Text>,
                  headerShown: false,
              }
          )
      },
      Content: {
          screen:ContentScreen,
          headerMode:'none',
          navigationOptions: (state) => (
              {
                  headerTitle: <Text style={{...globalStyles.h3}}>Content</Text>,
                  headerShown: false,
              }
          )
      },
      ContentCategories: {
          screen:CategoriesScreen,
          headerMode:'none',
          navigationOptions: (state) => (
              {
                  headerTitle: <Text style={{...globalStyles.h3}}>Content Categories</Text>,
                  headerShown: false,
              }
          )
      }
  }
);

HomeStack.navigationOptions = ({ navigation })=> ({
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused,tintColor }) => (
<AntDesign name='home' color={tintColor} size={20}/>
  ),
    tabNavigator:false,
    tabBarVisible: tabbarVisible(navigation),
})

HomeStack.path = '';

// const ContentStack = createStackNavigator(
//   {
//     Links: LinksScreen,
//   },
//     {...config, tabBarIcon:({tintColor})=>{
//
//         }},
// );

// LinksStack.navigationOptions = {
//   tabBarLabel: 'Links',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'} />
//   ),
// };

// LinksStack.path = '';

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
  },
  config
);

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
    title:'Settings',
  tabBarIcon: ({ focused,tintColor }) => (
    <Ionicons
        name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
        color={tintColor}
        size={20}
        style={{ marginBottom: -3 }}
    />
      )
};

SettingsStack.path = '';


const DiaryStack = createStackNavigator(
  {
    DiaryList: DiaryListScreen,
    Diary: DiaryScreen,
  },
  config
);

DiaryStack.navigationOptions = {
  tabBarLabel: 'Diary',
    tabBarIcon: ({ tintColor }) => (
        <Ionicons
            name={Platform.OS === 'ios' ? 'ios-journal' : 'md-journal'}
            color={tintColor}
            size={20}
            style={{ marginBottom: -3 }}
        />
    )
};

DiaryStack.path = '';

const TodoStack = createStackNavigator(
    {
        TodoList: TodoListScreen,
        Todo: TodoScreen,
    },
    config
);

TodoStack.navigationOptions = {
    tabBarLabel: 'Todo List',
    tabBarIcon: ({ tintColor }) => (
        <Octicons
            size={20}
            style={{ marginBottom: -3 }}
            color={tintColor}
            name={'tasklist'} />
    ),
};

TodoStack.path = '';

const tabbarVisible = (navigation) => {
    const { routes } = navigation.state;

    let showTabbar = true;
    routes.forEach((route) => {
        if ( route.routeName === 'Reflection'
            || route.routeName === 'Reflections'
            || route.routeName === 'Content'
        ) {

            showTabbar = false;
        }
    });

    return showTabbar;
};

const tabNavigator = createBottomTabNavigator({
        HomeStack,
        DiaryStack,
        TodoStack,
        SettingsStack
},
    {
        navigationOptions: ({ navigation }) => ({
            tabBarVisible: false,
        }),
        tabBarOptions:{
            tintColor:'#FB9D5D',
            activeTintColor:'#FB9D5D',
            inactiveColor:'#333'
        }
    },
);

tabNavigator.path = '';

export default tabNavigator;
