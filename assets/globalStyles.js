var styles = {
    h1:{
        fontSize:40,
        fontWeight: "100",
        fontFamily: 'gilroy-bold'
    },
    h2:{
        fontSize:30,
        fontWeight: "100",
        fontFamily: 'gilroy-bold'
    },
    h3:{
        fontSize:20,
        fontWeight: "100",
        fontFamily: 'gilroy-light'
    },
    h3Bold:{
        fontSize:20,
        fontWeight: "100",
        fontFamily: 'gilroy-bold'
    },
    h4:{
        fontSize:15,
        fontWeight: "100",
        fontFamily: 'gilroy-light'
    },
    h4Bold:{
        fontSize:15,
        fontWeight: "100",
        fontFamily: 'gilroy-bold'
    },
    h5:{
        fontSize:13,
        fontWeight: "100",
        fontFamily: 'gilroy-light'
    },
    textWhite:{
        color:'#fff'
    },
    textDark:{
        color:'#2D2B4A'
    },
    bg1:{
        backgroundColor:'#EB3E3A'
    },
    bg2:{
        backgroundColor:'#2D2B4A'
    },
    bgWhite:{
        backgroundColor:'#fff'
    },
    brandname: {
        fontSize: 50,
        color: '#fff'
    },
    button:{
        height:40,
        padding:15,
        marginTop:20,
        borderRadius:5,
        alignItems:'center',
        justifyContent:'center',
    },
}

export default styles;