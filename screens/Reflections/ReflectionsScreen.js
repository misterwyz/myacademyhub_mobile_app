import React, {useEffect} from "react";
import {StyleSheet, Platform, FlatList, ImageBackground, View, Text, RefreshControl,ScrollView} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {AnimatableManager} from 'react-native-ui-lib';
import {list} from "../../actions/reflectionActions"; //eslint-disable-line
import { connect } from 'react-redux';
import {TextBold} from "../../components/TextBold";
import globalStyles from "../../assets/globalStyles";
import {EvilIcons, MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";

const ReflectionsScreen = (props)=>{
    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = () => {
        setRefreshing(true);
        props.list();
    };

    useEffect(()=>{

        if(props.reflection) {
            setRefreshing(false);

        }else{

        }
    }, [props.reflection])

    useEffect(()=>{

        if (!props.auth){
            props.navigation.navigate('Auth');
        }

        props.list();

    }, [])

    const renderRow = function(row, id) {
        const animationProps = AnimatableManager.presets.fadeInBottom;
        return (
            <Animatable.View {...animationProps}

            >
                <View
                    key={id}
                    activeBackgroundColor='transparent'
                    activeOpacity={0.3}
                    height={290}
                    onPress={() => {props.navigation.navigate('Reflection', {audio:row})}}
                >
                    <View style={{...styles.overview}}  key={id}>
                    <ImageBackground
                        style={{
                            marginBottom: 15,
                            backgroundColor:'transparent',
                            height:'100%',
                            width:'100%',
                            justifyContent: 'flex-start',
                            flexDirection: 'column',
                            elevation:5
                        }}
                        imageStyle={{borderRadius:Platform.OS === 'ios' ? 10/2 : 10}}
                        source={{uri:'https://myacademyhub.s3.amazonaws.com/images/'+row.image_link}}
                        onPress={() => {props.navigation.navigate('Reflection', {audio:row})}}
                    >
                        <View style={{
                            flex:1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor:'rgba(000,000,000,0.5)',
                            borderRadius:Platform.OS === 'ios' ? 10/2 : 10,
                            paddingLeft:15,
                            paddingRight:15,
                        }}>
                            <TextBold style={{color:'#fff', fontSize:20,textAlign:"center"}}>
                                {row.author}
                            </TextBold>
                            <Text style={{...globalStyles.h3,color:'#fff',fontWeight:"500", textAlign:"center"}}>
                                {row.title}
                            </Text>
                            <Text style={{...globalStyles.h4,color:'#fff',fontWeight:"500", textAlign:"center"}}>
                                <MaterialCommunityIcons name='calendar-clock' size={13}/>
                                {" "}{moment(row.date).format('DD/MM/YYYY')}
                            </Text>

                            <EvilIcons name='play' size={52} color="white" onPress={() => {props.navigation.navigate('Reflection', {audio:row})}}/>
                        </View>
                    </ImageBackground>
                    </View>
                </View>
            </Animatable.View>

        );
    }

        return (
            <ScrollView style={{flex:1}}
                  refreshControl={
                      <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                  }
            >
            <FlatList
                data={props.reflection.reflections}
                renderItem={({item, index}) => renderRow(item, index)}
                keyExtractor={(item, index) => index.toString()}
            />
            </ScrollView>

        );

}

const styles = StyleSheet.create({
    image: {
        width: 54,
        height: 54,
        borderRadius: 20,
        marginHorizontal: 14,
    },
    border: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: '#ececec',
    },
    overview:{
        flex:1,
        marginTop:20,
        paddingTop:20,
        paddingLeft:15,
        paddingRight:15,
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
    },
});

const mapStateToProps = (state) => {
    const { auth,reflection } = state;
    return {auth,reflection}
};

export default connect(mapStateToProps, {list})(ReflectionsScreen);