import React from "react";
import {
    TouchableOpacity,
    ImageBackground,
    Text,
    View,
    StyleSheet,
    ScrollView,
    RefreshControl,
    SafeAreaView

}
    from "react-native";
import { connect } from 'react-redux';
const Dimensions = require('Dimensions');
const {height} = Dimensions.get('window');
import globalStyles from "../../assets/globalStyles";
import {categoryList} from "../../actions/contentActions";
import {
    SkypeIndicator
} from 'react-native-indicators';
const CategoriesScreen = (props)=>{
    const [categories, setCategories] = React.useState(undefined);
    const [refreshing, setRefreshing] = React.useState(false);
    React.useEffect(()=>{
        props.categoryList();

    }, []);

    const onRefresh = () => {
        setRefreshing(true);
        props.categoryList();
    };

    React.useEffect(()=>{

        if(props.content) {
            setRefreshing(false);
            if (props.content.categories) {
                if (props.content.categories.length) {

                    setCategories(props.content.categories)
                } else {
                    setCategories(undefined)
                }
            }
            else{
                setCategories([])
            }
        }else{
            setCategories(undefined)
        }
    }, [props.content])


    return(
        <SafeAreaView style={{flex:1}}>
        <ScrollView style={{flex:1}}
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
        >

        <View style={{...styles.firstActions}}>
            {
                categories !== undefined && !refreshing ?
                    categories.length > 0 ?
                        categories.map( (item, i) =>
                            (
                                <TouchableOpacity
                                    key={i}
                                    onPress={()=>props.navigation.navigate('Content',{name:item.name ? item.name : "N/A"})} style={[styles.categoryItem]}>
                                    <ImageBackground
                                        style={{
                                            backgroundColor:'transparent',
                                            height:'100%',
                                            width:'100%',
                                            justifyContent:'flex-start',
                                            alignItems:'flex-end',
                                            paddingBottom:5,
                                            paddingLeft:5,
                                            flexDirection:"row",
                                        }}
                                        source={require('../../assets/images/leadership.jpg')}
                                        resizeMode={'stretch'}
                                        borderRadius={10}
                                    >

                                        <View>
                                            <Text style={{color:'#fff', fontSize:20, ...globalStyles.h3Bold}}>
                                                {item.name ? item.name : ""}
                                            </Text>
                                        </View>
                                    </ImageBackground>
                                </TouchableOpacity>

                            )     )
                        : <Text>No Content Categories</Text>

                    : <View style={{justifyContent:'center', alignItems:'center', flex:1, flexDirection:'row', paddingTop:height/2 - 20}}>
                        <SkypeIndicator color='#FE9227' />
                    </View>
            }
        </View>
        </ScrollView>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    ...globalStyles,
    categoryItem:{
        // flex:3,
        marginBottom: 10,
        backgroundColor:'transparent',
        height:150,
        width:'45%',
        justifyContent:'flex-start',
        alignContent:'flex-end',
        paddingBottom:5,
        flexDirection:"row",
        elevation:5,
        padding:10
    },
    firstActions:{
        flexWrap:'wrap',
        flex:1,
        // width: '100%',
        flexDirection:'row',
        justifyContent:'space-around',
        alignContent:'flex-start'
    }
});


const mapStateToProps = (state) => {
    const {content } = state;
    return {content}
};

export default connect(mapStateToProps, {categoryList})(CategoriesScreen);