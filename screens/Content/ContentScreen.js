import React from "react";
import {Text,ImageBackground, Image,ScrollView,StyleSheet} from "react-native";;
import globalStyles from "../../assets/globalStyles";
import {Colors, Typography, View, Button} from 'react-native-ui-lib';
const Dimensions = require('Dimensions');
import { TabView, SceneMap } from 'react-native-tab-view';
const { width, height } = Dimensions.get('window');
import { TabBar } from 'react-native-tab-view';

const AuthorsRoute = () => (
    <View style={[ {flex:1, backgroundColor: '#ff4081' }]} />
);

const AboutRoute = () => (
    <View style={[{ flex:1,backgroundColor: '#673ab7' }]} />
);

const initialLayout = { width: Dimensions.get('window').width };

const renderTabBar = props => (
    <TabBar
        {...props}
        activeColor='#FEF2EA'
        indicatorStyle={{borderBottomColor:'#FA7921', borderBottomWidth:1 }}
        style={{ backgroundColor: 'transparent'}}
        renderLabel={({ route, focused, color }) => (
            <Text style={{ color:'#FA7921', margin: 8 }}>
                {route.title}
            </Text>
        )}
    />
);


const ContentScreen = ({navigation}) =>{
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Authors' },
        { key: 'second', title: 'About' },
    ]);

    const renderScene = SceneMap({
        first: AuthorsRoute,
        second: AboutRoute,
    });

    return(
        <ScrollView
            style={{flex:1,
            // overflow: 'visible'
            }}
                    showsVerticalScrollIndicator={false}
        >
            <View   style={{flex:1,
                justifyContent:"center",
                alignContent:"center",
                flexDirection:"row",
            }}>
            <ImageBackground
                style={{
                    height:height/2,
                    backgroundColor:'transparent',
                    width:width,
                    justifyContent:'flex-start',
                    alignItems:'flex-end',
                    paddingBottom:15,
                    paddingLeft:10,
                    flexDirection:"row",
                }}
                             source={require('../../assets/images/leadership.jpg')}
                             resizeMode={'stretch'}
            >

                <View>
                    <Text style={{color:'#fff', ...globalStyles.h3Bold}}>
                        {navigation.state.params.name}
                    </Text>
                </View>
            </ImageBackground>
            </View>
                <TabView
                    renderTabBar={renderTabBar}
                    navigationState={{ index, routes }}
                    renderScene={renderScene}
                    onIndexChange={setIndex}
                    initialLayout={initialLayout}
                />
        </ScrollView>
    )
};

ContentScreen.navigationOptions = {
    title: 'Content',
};


const styles = StyleSheet.create({
    tabbar: {
        marginVertical: 10
    }
});

export default ContentScreen