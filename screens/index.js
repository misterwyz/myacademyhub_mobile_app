import React from 'react';
import {ActivityIndicator,View} from "react-native";
import {authGetToken, logout, processLogin, setToken, setUser} from "../actions/authActions";
import { connect } from 'react-redux';

class InitialLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.bootstrapAsync();
    }

    bootstrapAsync = async () => {
        // Load the home screen for the logged in users
        this.props.authGetToken()
            .then( res=> {
                    // console.log(res)
                    if (res) {
                        this.props.navigation.navigate('Main');
                    }
                }
            )
            .catch(e=>{
                this.props.navigation.navigate('Auth');
            })

        // load the Auth screen if the user is NOT logged in

    }

    // Render any loading content that you like here
    render() {
        return (
            <View style={{flex:1}}>
                <ActivityIndicator />
            </View>
        );
    }
}




const mapStateToProps = (state) => {
    const { auth } = state;
    return {auth}
};

export default connect(mapStateToProps, {setUser, processLogin,setToken,authGetToken,logout})(InitialLoadingScreen);