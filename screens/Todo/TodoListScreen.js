import React, {useEffect, useState} from "react";
import {View, Text, StyleSheet, TextInput, TouchableOpacity, ScrollView, AsyncStorage} from "react-native";
import globalStyles from "../../assets/globalStyles";
import { MaterialCommunityIcons,EvilIcons } from '@expo/vector-icons';
import uuid from "uuid/v1";
import {
    SkypeIndicator
} from 'react-native-indicators';
import {Toast} from 'react-native-ui-lib';
const Dimensions = require('Dimensions');
const { width, height } = Dimensions.get('window');

const TodoListScreen = ({navigation}) =>{
    const [tasks, setTasks] = useState("");
    const [listName, setListName] = useState("");
    const [title, setTitle] = useState("Add a note to your dairy");
    const [message, setMessage] = useState("");
    const [toast, setToast] = useState(false);

    useEffect(()=>{
        setToast(false);
        // AsyncStorage.removeItem('huba_tasks',(e)=>{
        //     console.log(e)
        // })
        loadTasks().catch(e => {

        })
    },[]);

    const addList = async()  =>{
        try {
            if (listName === "" || listName.length < 2) {
                alert("That's too short")
                // setToast(true);
                return false;
            }
            const id = uuid();
            const new_list = {
                id,
                name:listName,
                tasks: null,
                createdAt: Date.now()
            }

            let task_list = await AsyncStorage.getItem('huba_tasks');

            if (task_list !== null) {
                //update item to array
                task_list = JSON.parse(task_list);
                task_list.push(new_list)
                await AsyncStorage.setItem('huba_tasks', JSON.stringify(task_list));
            } else {
                //add new item to array
                const new_task_list = [new_list]
                await AsyncStorage.setItem('huba_tasks',JSON.stringify(new_task_list));
                task_list = new_task_list;
            }

            setTasks(task_list || [])

            // setToast(true);
            setListName("")
        } catch (error) {
            alert(error.message)
            // setToast(true);
        }

    }

    const loadTasks = async () => {

        try {
            const allItems = await AsyncStorage.getItem('huba_tasks');
            // JSON.parse(allItems).map((item,i)=>{
            //     console.log(Object.keys(item))
            // })

            if (allItems){
                let filetred_notes = JSON.parse(allItems).filter(
                    item=> {
                        return  item != null }
                )

                filetred_notes = filetred_notes.filter(
                    item=> {
                        return  item != undefined }
                )


                setTasks(filetred_notes || [])
            }else{
                setTasks([])
            }

        } catch (err) {
            alert(err.message)
            // setToast(true);
        }
    };

    const deleteTask = async (key)=>{

        try {
            const allItems = await AsyncStorage.getItem('huba_tasks');


            let filtered_notes = JSON.parse(allItems).filter(
                item=> {
                    return  item != null }
            )



            if (filtered_notes){
                const transformed_notes = filtered_notes.filter(
                    item=>
                        item.id !== key

                )

                await AsyncStorage.setItem('huba_tasks', JSON.stringify(transformed_notes));

                setTasks(transformed_notes || {})
            }else{

            }

        } catch (err) {

            alert(err.message)
        }
    }

    return(
        <View style={{flex:1,padding:10}}>
            <Toast
                visible={toast}
                position={'top'}
                backgroundColor={'#FA7921'}
                color={'#fff'}
                message={message}
                onDismiss={() => setToast(false)}
                allowDismiss={true}
                // actions={closeToast}
            />
            <View style={styles.TouchableOpacityStyle}>
                <View style={{flex:5, justifyContent:'flex-start',alignItems:'center', flexDirection:'row'}}>
                    <TextInput style={[{paddingLeft: 10, width:'100%'}]} placeholder='Add List'
                               multiline={true}
                               onChangeText={(val)=>setListName(val)}
                    />
                </View>
                <TouchableOpacity style={{flex:1, justifyContent:'flex-end',flexDirection:'row'}} onPress={()=>addList()}>
                    <EvilIcons name='plus' color='#FE9227' size={24} onPress={()=>addList()}/>
                </TouchableOpacity>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>

                {
                    tasks !== undefined ?
                        tasks.length > 0 ?
                            tasks.map( (item, i) =>
                                (
                                    <TouchableOpacity key={i}
                                                      style={styles.task}
                                                      onPress={()=>navigation.navigate('Todo', {id:item.id})}
                                    >
                                        <View style={styles.task_item}>
                                            <View style={{flex:3, justifyContent:'flex-start',alignItems:'center', flexDirection:'row'}}>
                                                <Text style={[globalStyles.h3, {paddingLeft: 10}]}>{item.name}</Text>
                                            </View>
                                            <TouchableOpacity
                                                style={{flex:1, justifyContent:'flex-end',flexDirection:'row'}}
                                                onPress={()=>deleteTask(item.id)}
                                            >
                                                <MaterialCommunityIcons name='trash-can-outline' color='#333' size={24}/>
                                            </TouchableOpacity>
                                        </View>
                                    </TouchableOpacity>
                                )
                            )
                            :
                            <View style={{justifyContent:'center', alignItems:'center', flex:1, flexDirection:'row', paddingTop:height/2 - 20}}>
                                <Text>You haven't created any task yet</Text>
                            </View>
                        :
                        <View style={{justifyContent:'center', alignItems:'center', flex:1, flexDirection:'row', paddingTop:height/2 - 20}}>
                            <SkypeIndicator color='#FE9227' />
                        </View>
                }
            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    task:{
        width:'100%',
        padding:5,
        marginTop:10
    },
    task_item:{
        width:'100%',
        backgroundColor:'#fff',
        borderRadius:5,
        elevation:1,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:15,
        paddingTop:15,
        justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row",
        marginBottom:10
    },
    task_item_input:{
        width:'100%',
        backgroundColor:'#fff',
        borderRadius:5,
        elevation:1,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:15,
        paddingTop:15,
        // justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row",
        marginBottom:20
    },
    TouchableOpacityStyle:{
        width:width - 20,
        backgroundColor:'#fff',
        borderRadius:5,
        elevation:1,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:15,
        paddingTop:15,
        justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row",
        marginBottom:50

    },

    FloatingButtonStyle: {
        resizeMode: 'contain',
        width: 30,
        height: 30,
    }
})

TodoListScreen.navigationOptions = {
    title: 'Todo List',
};

export default TodoListScreen