import React, {useEffect, useState} from "react";
import {View, Text, StyleSheet, TextInput, TouchableOpacity, ScrollView, AsyncStorage} from "react-native";
import globalStyles from "../../assets/globalStyles";
import { MaterialIcons,MaterialCommunityIcons,EvilIcons } from '@expo/vector-icons';
import uuid from "uuid/v1";
import {Toast} from 'react-native-ui-lib';
const Dimensions = require('Dimensions');
const { width, height } = Dimensions.get('window');
import moment from "moment";


const TodoScreen = ({navigation}) =>{
    const [task, setTask] = useState(undefined);
    const [listName, setListName] = useState("");
    const [note, setNote] = useState("");
    const [message, setMessage] = useState("");
    const [toast, setToast] = useState(false);

    useEffect( ()=>{
        console.log('on mount')
        EnterScreen().catch(e=>console.log(e));
        LeaveScreen();
    },[]);


    const addNote = async()  =>{
        try {
            if (note === "" || note.length < 1){
                return false;
            }
            const allItems = await AsyncStorage.getItem('huba_tasks');

            if (allItems){
                let filtered_notes = JSON.parse(allItems).filter(
                    item=> {
                        return  item != null }
                )

                filtered_notes = filtered_notes.filter(
                    item=> {
                        return  item != undefined }
                )


                filtered_notes.map(
                    item=> {
                        if(item.id=== navigation.state.params.id){
                            if (item.tasks) {
                                item.tasks.push({
                                    id: uuid(),
                                    status: 'pending',
                                    text: note,
                                    createdAt: Date.now()
                                });
                            }else{
                                item.tasks = [{
                                    id: uuid(),
                                    status: 'pending',
                                    text: note,
                                    createdAt: Date.now()
                                }]
                            }
                        }
                    }
                )

                await AsyncStorage.setItem('huba_tasks', JSON.stringify(filtered_notes));
                const current_task = filtered_notes.find(item=>item.id === navigation.state.params.id)
                setTask(current_task)

            }else{
                setMessage("Error Creating task")
                setToast(true);
            }

        } catch (err) {
            console.log(err)
        }

    }

    const EnterScreen = async()=>{
        setToast(false);
        const removeFocus = navigation.addListener('didFocus', async () => {
            if(navigation.state.params !== undefined ){

                try {
                    const allItems = await AsyncStorage.getItem('huba_tasks');

                    if (allItems){
                        let filtered_tasks = JSON.parse(allItems).filter(
                            item=> {
                                return  item != null }
                        )

                        filtered_tasks = filtered_tasks.filter(
                            item=> {
                                return  item != undefined }
                        )

                        const transformed_note = filtered_tasks.find(
                            item=> item.id=== navigation.state.params.id
                        )


                        setListName(transformed_note.name || "")
                        setTask(transformed_note)

                    }else{
                        setListName("")
                    }

                } catch (err) {

                    setMessage(err.message)
                    setToast(true);
                }
            }
        });
        return removeFocus;
    }

    const LeaveScreen = ()=>{
        const removeBlur = navigation.addListener('willBlur', async () => {
            // setTitle("Add a note to your dairy");

        });
        return removeBlur;
    }


    const deleteTask = async (key)=>{
        // console.log('key ')
        // console.log(key)
        try {
            const allItems = await AsyncStorage.getItem('huba_tasks');

            if (allItems){

                const transformed_tasks = JSON.parse(allItems).map(
                    item=> {
                        if( item.id === navigation.state.params.id){
                            // console.log(item.id, navigation.state.params.id)
                            if(item.tasks) {
                                let tasks = [...item.tasks];
                                tasks = tasks.filter(task => task.id !== key);
                                // console.log('filtered task')
                                // console.log(tasks)
                                item.tasks = tasks;
                            }
                        }
                        return item
                    }
                )
                // console.log('transformed_tasks')
                // console.log(transformed_tasks)
                //
                //
                // console.log('allItems')
                // console.log(allItems)


                await AsyncStorage.setItem('huba_tasks', JSON.stringify(transformed_tasks));

                const transformed_note = await transformed_tasks.find(
                    item=> item.id=== navigation.state.params.id
                )
                setTask(transformed_note || undefined)

            }else{
                console.log('no items')
            }

        } catch (err) {
            console.log(err)
            setMessage(err.message)
            setToast(true);
        }
    }

    const updateTaskStatus = async (key)=>{
        // console.log('key ')
        // console.log(key)
        try {
            const allItems = await AsyncStorage.getItem('huba_tasks');

            if (allItems){

                const transformed_tasks = JSON.parse(allItems).map(
                    item=> {
                        if( item.id === navigation.state.params.id){
                            // console.log(item.id, navigation.state.params.id)
                            let tasks = [...item.tasks];
                            if(tasks) {
                                // console.log('tasks')
                                // console.log(tasks)
                                tasks = tasks.map(task => {
                                    // console.log('task before')
                                    // console.log(task)

                                    if (task.id === key){
                                        if(task.status === 'pending'){
                                            task.status = 'completed'
                                        }else{
                                            task.status = 'pending'
                                        }
                                    }
                                    // console.log('task after')
                                    // console.log(task)
                                    return task;
                                });
                                // console.log('filtered task')
                                // console.log(tasks)
                                item.tasks = tasks;
                                return item;
                            }
                        }
                        return item
                    }
                )
                // console.log('transformed_tasks')
                // console.log(transformed_tasks)
                //
                //
                // console.log('allItems')
                // console.log(allItems)


                await AsyncStorage.setItem('huba_tasks', JSON.stringify(transformed_tasks));

                const transformed_task = await transformed_tasks.find(
                    item=> item.id=== navigation.state.params.id
                )
                setTask(transformed_task || undefined)

            }else{
                console.log('no items')
            }

        } catch (err) {
            // console.log(err)
            setMessage(err.message)
            setToast(true);
        }
    }


    return(
        <View style={{flex:1,padding:10}}>
            <Toast
                visible={toast}
                position={'top'}
                backgroundColor={'#FA7921'}
                color={'#fff'}
                message={message}
                onDismiss={() => setToast(false)}
                allowDismiss={true}
                // actions={closeToast}
            />
            <ScrollView style={styles.task} showsVerticalScrollIndicator={false}>
                <View style={{marginBottom:10}}>
                <Text style={[globalStyles.h3]}>{listName}</Text>
                {task !== undefined ? (
                    <Text style={{...globalStyles.h4, color: '#333'}}>
                        <MaterialCommunityIcons name='calendar-clock' size={13}/>
                        {" "}{moment(task.createdAt).calendar()}
                    </Text>
                )
                    : null
                }
                </View>
                <View style={styles.task_item}>
                    <View style={{flex:5, justifyContent:'flex-start',alignItems:'center', flexDirection:'row'}}>
                        <TextInput
                            style={[{paddingLeft: 10, width:'100%'}]}
                            placeholder='Add Task'
                            multiline={true}
                            onChangeText={(val)=>setNote(val)}
                        />
                    </View>
                    <TouchableOpacity
                        style={{flex:1, justifyContent:'flex-end',flexDirection:'row'}}
                        onPress={()=>addNote()}
                    >
                        <EvilIcons name='plus' color='#FE9227' size={24}/>
                    </TouchableOpacity>
                </View>
                {
                    task !== undefined ?
                        task.hasOwnProperty('tasks') ? task.tasks !== null ?
                            task.tasks.map( (item, i) =>
                                (
                                <View key={i} style={{marginTop:10}}>
                                    <View style={styles.task_item}>
                                        <View style={{flex:3, justifyContent:'flex-start',alignItems:'center', flexDirection:'row'}}>
                                            <TouchableOpacity onPress={()=>updateTaskStatus(item.id)}>
                                            <MaterialIcons name={`${item.status === 'pending' ? 'check-box-outline-blank' : 'check-box'}`} color='#FE9227' size={24}/>
                                            </TouchableOpacity>
                                            <Text style={[globalStyles.h4, {paddingLeft: 10,textDecorationLine:`${item.status === 'pending' ? 'none' : 'line-through'}`}]}>{item.text}</Text>
                                        </View>
                                        <TouchableOpacity
                                            style={{flex:1, justifyContent:'flex-end',flexDirection:'row'}}
                                            onPress={()=>deleteTask(item.id)}
                                        >
                                            <MaterialCommunityIcons name='trash-can-outline' color='#333' size={20}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                )
                            )
                            // : <Text>You haven't added any note yet</Text>

                            :null
                                : null
                                    : null
                }
                </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    task:{
        width:'100%',
        padding:5
    },
    task_item:{
        width:'100%',
        backgroundColor:'#fff',
        borderRadius:5,
        elevation:1,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:15,
        paddingTop:15,
        justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row",
        marginBottom:10
    },
    task_item_input:{
        width:'100%',
        backgroundColor:'#fff',
        borderRadius:5,
        elevation:1,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:15,
        paddingTop:15,
        // justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row",
        marginBottom:20
    },
    TouchableOpacityStyle:{
        width:width - 20,
        backgroundColor:'#fff',
        borderRadius:5,
        elevation:1,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:15,
        paddingTop:15,
        justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row",
        marginBottom:50

    },

    FloatingButtonStyle: {
        resizeMode: 'contain',
        width: 30,
        height: 30,
    }
})

TodoScreen.navigationOptions = {
    title: 'Todo List',
};

export default TodoScreen