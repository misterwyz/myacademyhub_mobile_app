import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  ImageBackground,
  StatusBar,
  SafeAreaView,
  Image, AsyncStorage
} from 'react-native';

import {View } from 'react-native-ui-lib';
import {TextBold} from "../components/TextBold"; // eslint-disable-line
import { SimpleLineIcons, EvilIcons,AntDesign } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { today} from "../actions/reflectionActions";
import {categoryList} from "../actions/contentActions";
// import Carousel from 'react-native-snap-carousel';
// import sliderStyle from "./sliderStyle";
// import { sliderWidth, itemWidth } from './SliderEntry.style';
// import SliderEntry from "./SliderEntry";
const Dimensions = require('Dimensions');
import globalStyles from '../assets/globalStyles';
import { Drawer } from 'native-base';
import SideBar from "../components/SideBar";
import {logout} from "../actions/authActions";
import {
  SkypeIndicator
} from 'react-native-indicators';
const {height} = Dimensions.get('window');
const closeDrawer = ()=>{
  drawer._root.close()
}

const openDrawer = ()=>{
  drawer._root.open()
}
const HomeScreen = (props)=> {
  const [reflection, setReflection] = useState(undefined);
  const [categories, setCategories] = useState(undefined);


  useEffect(()=>{
    // props.logout();
    if (!props.auth){
      return props.navigation.navigate('Auth');
    }
    EnterScreen().catch(e => {
    console.log(e)
    })
    props.today();
    props.categoryList();

  }, [])

  useEffect(()=>{
    // props.logout();
    if (!props.auth){
      return props.navigation.navigate('Auth');
    }

    if(props.reflection) {
      if (props.reflection.reflection_for_today) {
          setReflection(props.reflection.reflection_for_today)
      }
      else{
        setReflection(undefined)
      }
    }else{
      setReflection(undefined)
    }


    if(props.content) {
      if (props.content.categories) {
        if (props.content.categories.length) {

          setCategories(props.content.categories)
        } else {
          setCategories(undefined)
        }
      }
      else{
        setCategories([])
      }
    }else{
      setCategories(undefined)
    }
  }, [props.reflection, props.content, props.auth])

  const EnterScreen = async()=>{

    const removeFocus = props.navigation.addListener('didFocus', async () => {
      console.log('entered screen')
      closeDrawer()
    });
    return removeFocus;
  }

const logout =()=>{
    props.logout();
   props.navigation.navigate('Auth');

}


  return (
      <Drawer
          style={{ zIndex: 50,flex:1,width: '40' }}
          ref = {(ref) => {drawer = ref} }
          openDrawerOffset={150}
          type="displace"
          acceptPan={true}
          captureGestures={true}
          content={<SideBar
              navigator={props.navigation}
              name={props.auth ? props.auth.user ? props.auth.user.data ? props.auth.user.data.firstname : "" : "" : ""}
              logout={logout}
          />
          }
          onClose={ ()=> closeDrawer()}
      >
        <SafeAreaView style={{flex:1,backgroundColor:'#FEF2EA',}}>
        <View style={{
          width:'100%',
          backgroundColor:'#FE9227',
          justifyContent: 'center',
          alignContent:'center',
          paddingLeft:15,
          paddingRight:20,
          paddingTop:15,
          height: StatusBar.currentHeight*3,
        }}>
          <Text style={{...globalStyles.h3,color:"#fff", fontWeight:"400"}} onPress={() => openDrawer()}>
            {/*<AntDesign name={'user'} size={28} color="#2D2B4A" onPress={() => openDrawer()}/>*/}
            <Image source={require('../assets/images/list.png')} style={{width:20, height:20}} onPress={() => openDrawer()}/>
          </Text>
        </View>
    <View style={styles.container}>
      <ScrollView scrollEventThrottle={16}>
        <View style={{...styles.overview}}>
            {
              reflection != undefined ?
                  (<ImageBackground
                        style={{
                          marginBottom: 15,
                          backgroundColor:'transparent',
                          height:'100%',
                          width:'100%',
                          justifyContent: 'flex-start',
                          flexDirection: 'column',
                        }}
                        source={{uri:'https://myacademyhub.s3.amazonaws.com/images/'+reflection.image_link}}
                        onPress={() => {props.navigation.navigate('Reflection', {audio:reflection})}}
                      >
                    <View style={{
                      flex:1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor:'rgba(000,000,000,0.5)',
                      paddingLeft:15,
                      paddingRight:15
                    }}>
                      <TextBold style={{color:'#fff', fontSize:20}}>
                        {reflection.author}
                      </TextBold>
                      <Text style={{...globalStyles.h3,color:'#fff',fontWeight:"500",textAlign:'center'}}>
                        {reflection.title}
                      </Text>
                      <Text style={{...globalStyles.h4,color:'#fff',textAlign:'center'}}>
                        {reflection.content.slice(0,200)+ '...'}
                        {"\n"}
                      </Text>

                      <EvilIcons name='play' size={52} color="white" onPress={() => {props.navigation.navigate('Reflection', {audio:reflection})}}/>
                    </View>
                  </ImageBackground>

              )

                  : <View style={{justifyContent:'center', alignItems:'center', flex:1, flexDirection:'column'}}>
                    <SkypeIndicator color='#FE9227' />
                  </View>

            }
        </View>

        <View style={{flex:1,
          backgroundColor: '#FE9227',
          paddingTop:10,
          paddingBottom: 10,
          paddingLeft:10,
          justifyContent:'space-between',
          alignItems:'center',
          flexDirection:'row',
          paddingRight:10}
        }>
          <View>
             <Text style={{...globalStyles.h3,color:'#fff'}}>Nice to see you, {props.auth ? props.auth.user.data.firstname : ""}</Text>
          <TouchableOpacity
              onPress={()=>props.navigation.navigate('Diary',{ready:true})}>
            <Text style={{...globalStyles.h5, color:'#fff', marginTop:10}}>Have anything on your mind?</Text>
          </TouchableOpacity>
          </View>

          <View>
            <TouchableOpacity
                onPress={()=>props.navigation.navigate('Diary',{ready:true})}
                style={{
              height:40,
              width:80,
              borderRadius:5,
              alignItems:'center',
              justifyContent: 'center',
              backgroundColor:'#fff',
            }}>
              <Text style={[globalStyles.h4,{color:'#333'}]}>Diary</Text>
            </TouchableOpacity>
          </View>
        </View>

<View style={{flex:1,justifyContent:'space-between',flexDirection:'row', padding:20}}>
  <Text style={{...globalStyles.h3Bold}}>Categories</Text>
  <TouchableOpacity
      onPress={()=>props.navigation.navigate('ContentCategories',{ready:true})}
  >
    <Text style={{...globalStyles.h4}}>View All</Text></TouchableOpacity>
</View>
        <View style={{...styles.firstActions}}>
          <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
          >
          {
            categories !== undefined ?
                categories.length > 0 ?
                    categories.map( (item, i) =>
                    (
                        <TouchableOpacity
                            key={i}
                            onPress={()=>props.navigation.navigate('Content',{name:item.name ? item.name : "N/A"})} style={[styles.categoryItem]}>
                          <ImageBackground
                              style={{
                                backgroundColor:'transparent',
                                height:'100%',
                                width:'100%',
                                justifyContent:'flex-start',
                                alignItems:'flex-end',
                                paddingBottom:5,
                                paddingLeft:5,
                                flexDirection:"row",
                              }}
                              source={require('../assets/images/leadership.jpg')}
                                         resizeMode={'stretch'}
                                         borderRadius={10}
                        >

                          <View>
                            <Text style={{color:'#fff', fontSize:20, ...globalStyles.h3Bold}}>
                              {item.name ? item.name : ""}
                            </Text>
                          </View>
                        </ImageBackground>
                        </TouchableOpacity>

                    )     )
                    : <Text>No Content categories</Text>

                : null
              }
          </ScrollView>
          {
            categories == undefined ?
               null
                    : null
          }
        </View>
      </ScrollView>
        </View>
        </SafeAreaView>
</Drawer>
  );
}

HomeScreen.navigationOptions = {
  header: null,
  //       headerTitle: ()=> <SimpleLineIcons name={'menu'} size={18} color="#2D2B4A" style={{marginRight:10}} onPress={() => openDrawer()}/>,
  // headerTitle: (
  //     <View
  //         style={{
  //           flex: 1,
  //           backgroundColor: 'transparent',
  //           zIndex:1,
  //           alignItems: 'center',
  //           flexDirection: 'row',
  //           paddingHorizontal: 10,
  //           height: StatusBar.currentHeight,
  //         }}>
  //       <SimpleLineIcons name={'menu'} size={18} color="#2D2B4A" style={{marginRight:10}} onPress={() => openDrawer()}/>
  //     </View>
  // ),
  //       headerShown: true,
  // headerTintColor: '#fff',
  // headerTitleStyle: {
  //   fontWeight: 'bold',
  // },
};


const styles = StyleSheet.create({
  ...globalStyles,
  container:{
    flex:1
  },
  overview:{
    height:height/2 - 20,
    justifyContent:'center',
    alignItems:'center',
    marginBottom: -10
  },
  categoryItem:{
    // flex:3,
    marginBottom: 10,
    backgroundColor:'transparent',
    height:150,
    width:'20%',
    justifyContent:'flex-start',
    alignContent:'flex-end',
    paddingBottom:5,
    flexDirection:"row",
    elevation:5,
    padding:10
  },
  firstActions:{
    flexWrap:'wrap',
    flex:1,
    // width: '100%',
    flexDirection:'row',
    justifyContent:'space-around',
    alignContent:'flex-start'
  }
});

const mapStateToProps = (state) => {
  const { auth,reflection,content } = state;
  return {auth,reflection,content}
};

export default connect(mapStateToProps, {today,logout,categoryList})(HomeScreen);