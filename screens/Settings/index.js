import React, { Component } from 'react'
import { ScrollView, Switch, StyleSheet, Text, View } from 'react-native'
import { Avatar, ListItem } from 'react-native-elements'
import { connect } from 'react-redux';
import InfoText from '../../components/InfoText'
import { EvilIcons,MaterialIcons,MaterialCommunityIcons } from '@expo/vector-icons';

import {logout} from "../../actions/authActions";


const styles = StyleSheet.create({
    scroll: {
        backgroundColor: 'white',
    },
    userRow: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingBottom: 8,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 6,
    },
    userImage: {
        marginRight: 12,
    },
    listItemContainer: {
        height: 55,
        borderWidth: 0.5,
        borderColor: '#ECECEC',
    },
})



class SettingsScreen extends Component {

    state = {
        pushNotifications: true,
    }

    onPressOptions = () => {
        this.props.navigation.navigate('options')
    }

    onChangePushNotifications = () => {
        this.setState(state => ({
            pushNotifications: !state.pushNotifications,
        }))
    }

    render() {
        const { avatar, firstname, email } = this.props.auth.user.data
        return (
            <ScrollView style={styles.scroll}>
                <View style={styles.userRow}>
                    <View style={styles.userImage}>
                        <Avatar
                            rounded
                            size="large"
                            source={{
                                uri: avatar,
                            }}
                        />
                    </View>
                    <View>
                        <Text style={{ fontSize: 16 }}>{firstname}</Text>
                        <Text
                            style={{
                                color: 'gray',
                                fontSize: 16,
                            }}
                        >
                            {email}
                        </Text>
                    </View>
                </View>
                <InfoText text="Account" />
                <View>
                    <ListItem
                        hideChevron
                        title="Push Notifications"
                        containerStyle={styles.listItemContainer}
                        rightElement={
                            <Switch
                                onValueChange={this.onChangePushNotifications}
                                value={this.state.pushNotifications}
                            />
                        }
                        leftIcon={
                            <MaterialIcons name='notifications' size={20}/>
                        }
                    />

                    <ListItem
                        title="Location"
                        rightTitle="Lagos"
                        rightTitleStyle={{ fontSize: 15 }}
                        onPress={() => this.onPressOptions()}
                        containerStyle={styles.listItemContainer}
                        leftIcon={
                            <MaterialIcons name='place' size={20}/>
                        }
                        rightIcon={<EvilIcons name='chevron-right' size={20}/>}
                    />

                </View>
                <InfoText text="More" />
                <View>
                    <ListItem
                        title="About US"
                        onPress={() => this.onPressOptions()}
                        containerStyle={styles.listItemContainer}
                        leftIcon={
                            <MaterialCommunityIcons name='information' size={20}/>
                        }
                        rightIcon={<EvilIcons name='chevron-right' size={20}/>}
                    />
                    <ListItem
                        title="Terms and Policies"
                        onPress={() => this.onPressOptions()}
                        containerStyle={styles.listItemContainer}
                        leftIcon={
                            <MaterialCommunityIcons name='lightbulb' size={20}/>
                        }
                        rightIcon={<EvilIcons name='chevron-right' size={20}/>}
                    />
                    <ListItem
                        title="Share our App"
                        onPress={() => this.onPressOptions()}
                        containerStyle={styles.listItemContainer}
                        leftIcon={
                            <MaterialIcons name='share' />
                        }
                        rightIcon={<EvilIcons name='chevron-right' size={20}/>}
                    />
                    {/*<ListItem*/}
                    {/*    title="Rate Us"*/}
                    {/*    onPress={() => this.onPressOptions()}*/}
                    {/*    containerStyle={styles.listItemContainer}*/}
                    {/*    badge={{*/}
                    {/*        value: 5,*/}
                    {/*        textStyle: { color: 'white' },*/}
                    {/*        containerStyle: { backgroundColor: 'gray', marginTop: 0 },*/}
                    {/*    }}*/}
                    {/*    leftIcon={*/}
                    {/*        <EvilIcons name='star' />*/}
                    {/*    }*/}
                    {/*    rightIcon={<EvilIcons name='chevron-right' />}*/}
                    {/*/>*/}
                    {/*<ListItem*/}
                    {/*    title="Send FeedBack"*/}
                    {/*    onPress={() => this.onPressOptions()}*/}
                    {/*    containerStyle={styles.listItemContainer}*/}
                    {/*    leftIcon={*/}
                    {/*        <EvilIcons name='feedback' />*/}
                    {/*    }*/}
                    {/*    rightIcon={<EvilIcons name='chevron-right' />}*/}
                    {/*/>*/}
                </View>
            </ScrollView>
        )
    }
}

SettingsScreen.navigationOptions = {
    title: 'Settings',
};

const mapStateToProps = (state) => {
    const { auth } = state;
    return {auth}
};

export default connect(mapStateToProps, {logout})(SettingsScreen);