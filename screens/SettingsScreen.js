import React from 'react';
import {View,Text} from "react-native";

export default  SettingsScreen = ()=> {

  return(
      <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
        <Text>Settings</Text>
      </View>
  )
}

SettingsScreen.navigationOptions = {
  title: 'Settings',
};
