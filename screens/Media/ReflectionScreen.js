import React, {useEffect, useState} from "react";
import { Audio } from 'expo-av';
import {View,Text,StyleSheet, TouchableOpacity,Image,ScrollView,Platform} from "react-native";
import {EvilIcons, Ionicons} from '@expo/vector-icons';
import globalStyles from '../../assets/globalStyles';
import {TextBold} from "../../components/TextBold";
import Spinner from "../../components/Spinner";

export default  (props)=>{
    const [audio , setAudio] = useState({});
    const [isPlaying , setIsPlaying] = useState(false);
    const [playbackInstance , setPlaybackInstance] = useState(null);
    const [currentIndex , setCurrentIndex] = useState(0);
    const [volume , setVolume] = useState(1.0);
    const [isBuffering , setIsBuffering] = useState(false);
    const [shouldReplay , setShouldReplay] = useState(false);

    useEffect(  ()=>{

        if(props.navigation.state.params.audio){
            setAudio(props.navigation.state.params.audio)
            playAudio().catch(e=>console.log(e));
        }

    }, [props.audio])

    useEffect( ()=>{
        screenManager();
        EnterScreen();
        LeaveScreen();
    })

    const playAudio = async ()=>{

            await Audio.setAudioModeAsync({
                allowsRecordingIOS: false,
                interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
                playsInSilentModeIOS: true,
                interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DUCK_OTHERS,
                shouldDuckAndroid: true,
                staysActiveInBackground: true,
                playThroughEarpieceAndroid: false
            })
           await loadAudio()

    }

    const  loadAudio = async () => {
        const reflection = props.navigation.state.params.audio

        try {
            const playbackInstance = new Audio.Sound()
            const source = {
                uri: 'https://myacademyhub.s3.amazonaws.com/audio/'+reflection.audio_link
            }
            const status = {
                shouldPlay: isPlaying,
                volume
            }
            playbackInstance.setOnPlaybackStatusUpdate(onPlaybackStatusUpdate)
            await playbackInstance.loadAsync(source, status, false)
            setPlaybackInstance(playbackInstance)
        } catch (e) {
            // console.log(e)
        }
    }

   const onPlaybackStatusUpdate = status => {
        setIsBuffering(status.isBuffering)
       if (status.didJustFinish && !status.isLooping) {
           setIsPlaying(false);
           setShouldReplay(true);
       }
    }

    const handlePlayPause = async () => {
        try {
            if(shouldReplay && isPlaying){
                await playbackInstance.pauseAsync();
                setIsPlaying(false);
                setShouldReplay(false);
            }else if(shouldReplay && !isPlaying){
            await playbackInstance.replayAsync();
                setIsPlaying(true);
                setShouldReplay(false);
            }else if(!shouldReplay && !isPlaying){
            await playbackInstance.playAsync()
                setIsPlaying(true)
            }else if(!shouldReplay && isPlaying){
            await playbackInstance.pauseAsync()
                setIsPlaying(false)
            }
        }catch (e) {
            setIsBuffering(true)
        }
    }

    // const handlePreviousTrack = async () => {
    //     let { playbackInstance, currentIndex } = this.state
    //     if (playbackInstance) {
    //         await playbackInstance.unloadAsync()
    //         currentIndex < audioBookPlaylist.length - 1
    //             ? (currentIndex -= 1)
    //             : (currentIndex = 0)
    //         this.setState({
    //             currentIndex
    //         })
    //         this.loadAudio()
    //     }
    // }
    // const handleNextTrack = async () => {
    //     let { playbackInstance, currentIndex } = this.state
    //     if (playbackInstance) {
    //         await playbackInstance.unloadAsync()
    //         currentIndex < audioBookPlaylist.length - 1
    //             ? (currentIndex += 1)
    //             : (currentIndex = 0)
    //         this.setState({
    //             currentIndex
    //         })
    //         this.loadAudio()
    //     }
    // }

    const screenManager = ()=>{
        // props.navigation.navigationOptions =  ( props ) => ({
        //
        //     headerTitle: () => <Text style={{...globalStyles.h3}}>Test</Text>,
        //     headerRight:
        //         <Button
        //             // onPress={navigation.getParam('increaseCount')}
        //             title="Close"
        //             color="#333"
        //         />
        //
        // })
    }

    const EnterScreen = ()=>{

        const removeFocus = props.navigation.addListener('willFocus', () => {
            // console.log('entered screen')
        });
        return removeFocus;
    }

    const LeaveScreen = ()=>{
        const removeBlur = props.navigation.addListener('willBlur', async () => {
            if(isPlaying){
                await playbackInstance.pauseAsync()
                setIsPlaying( false)
            }

        });
        return removeBlur;
    }



    return(
        <ScrollView>
            <View style={styles.container}>

            <TextBold style={{fontSize:22, textAlign: 'center'}}>
                {audio.title}
            </TextBold>
                <TextBold style={{fontSize:20,textAlign: 'center', marginBottom:15,}}>
                    {audio.author}
                </TextBold>
                <View style={{        elevation:5,
                    borderRadius:Platform.OS === 'ios' ? 30/2 : 30}}>
            <Image
                style={styles.albumCover}
                source={{
                    uri: 'https://myacademyhub.s3.amazonaws.com/images/'+audio.image_link
                }}
            />
                </View>

            <View style={styles.controls}>
                {/*<TouchableOpacity style={styles.control} onPress={() => alert('')}>*/}
                {/*    <Ionicons name="ios-skip-backward" size={48} color="#444" />*/}
                {/*</TouchableOpacity>*/}
                {isBuffering ? <Spinner color={'#FA7921'}/> : (
                    <TouchableOpacity style={styles.control} onPress={() => handlePlayPause()}>
                        {isPlaying ? (
                            <Ionicons name="ios-pause" size={48} color="#444"/>
                        ) : (

                            <EvilIcons name='play' size={52} color="#333" />
                        )}
                    </TouchableOpacity>
                )
                }
                {/*<TouchableOpacity style={styles.control} onPress={() => alert('')}>*/}
                {/*    <Ionicons name="ios-skip-forward" size={48} color="#444" />*/}
                {/*</TouchableOpacity>*/}
            </View>
            <View style={styles.trackInfo}>
                <Text style={[globalStyles.h4]}>
                    {audio.content}
                </Text>

            </View>
            </View>
        </ScrollView>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop:20,
        paddingBottom:20,
    },
    albumCover: {
        width: 250,
        height: 250,
        borderRadius:Platform.OS === 'ios' ? 30/2 : 30
    },
    trackInfo: {
        padding: 40,
        backgroundColor: '#fff'
    },
    trackInfoText: {
        textAlign: 'center',
        flexWrap: 'wrap',
        color: '#550088'
    },
    largeText: {
        fontSize: 22
    },
    smallText: {
        fontSize: 16
    },
    control: {
        marginTop: 20
    },
    controls: {
        flexDirection: 'row'
    }
})

