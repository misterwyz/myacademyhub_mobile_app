import React, {useState, useEffect} from "react";
import {AsyncStorage,StyleSheet,View,Text,TouchableOpacity,Image,Alert,ScrollView} from 'react-native'
const Dimensions = require('Dimensions');
const { width, height } = Dimensions.get('window');
import {Colors, FloatingButton,Toast} from 'react-native-ui-lib';
import globalStyles from "../../assets/globalStyles";
import moment from "moment";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

const DiaryListScreen = ({navigation}) =>{

    const [notes, setNotes] = useState("");
    const [message, setMessage] = useState("");
    const [toast, setToast] = useState(false);

    useEffect(()=>{
        loadDiary().catch(e => {
            setMessage(e.message)
            setToast(true)
        })
    },[]);

    useEffect( ()=>{
        EnterScreen();
    })

    const EnterScreen = ()=>{
        const removeFocus = navigation.addListener('didFocus', () => {
            loadDiary().catch(e => {
                setMessage(e.message)
                setToast(true)
            })
        });
        return removeFocus;
    }

    const onSwipeLeft = (gestureState, id)=>{

        Alert.alert(
            'Delete note',
            '',
            [
                {
                    text: 'Cancel',
                    onPress: () => {
                        setMessage("Action Cancelled")
                        setToast(true)
                    },
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => deleteNote(id)},
            ],
            {cancelable: false},
        );
    }


    const onSwipeRight = (gestureState, id)=>{
        navigation.navigate('Diary', {id:id})
    }

    const deleteNote = async (key)=>{

        try {
            const allItems = await AsyncStorage.getItem('huba_diary');

            if (allItems){
                c
                const filetred_notes = JSON.parse(allItems).filter(
                    item=> {
                        let current_obj = Object.keys(item)
                        return item[current_obj].text !== undefined }
                )

                const transformed_notes = filetred_notes.filter(
                    item=> {
                        let current_obj = Object.keys(item)
                        return current_obj[0] !== key
                    }
                )
                await AsyncStorage.setItem('huba_diary', JSON.stringify(transformed_notes));

                setNotes(transformed_notes || [])
                setMessage("Note deleted")
                setToast(true);
            }else{
                setNotes([])
            }

        } catch (err) {
            setMessage(err.message)
            setToast(true);
        }
    }

    const loadDiary = async () => {

        try {
            const allItems = await AsyncStorage.getItem('huba_diary');
            // JSON.parse(allItems).map((item,i)=>{
            //     console.log(Object.keys(item))
            // })

            if (allItems){
                let filetred_notes = JSON.parse(allItems).filter(
                    item=> {
                        return  item != null }
                )

                 filetred_notes = filetred_notes.filter(
                    item=> {
                        return  item != undefined }
                )

                 filetred_notes = filetred_notes.filter(
                    item=> {
                        let current_obj = Object.keys(item)
                        return item[current_obj].text !== undefined }
                    )

                const transformed_notes = filetred_notes.map(
                    item=> {
                        let current_obj = Object.keys(item)
                        return {
                            id:current_obj[0],
                            text: item[current_obj].text.slice(0,120)+ '...' ,
                            createdAt: item[current_obj].createdAt
                        }
                    }
                )

                setNotes(transformed_notes || [])
            }else{
                setNotes([])
            }

        } catch (err) {
            setMessage(err.message)
            setToast(true);
        }
    };

    return(
        <View style={{flex:1,
            height:height
        }}>
            <Toast
                visible={toast}
                position={'top'}
                backgroundColor={'#FA7921'}
                color={'#fff'}
                message={message}
                onDismiss={() => setToast(false)}
                allowDismiss={true}
                // actions={closeToast}
            />
            <ScrollView style={{
                flex:1,
                flexDirection:"column",
                paddingLeft:15,
                marginRight:15}
            }>

            <View style={{marginBottom:20, marginTop:20}}>
                <Text style={{...globalStyles.h3}}>My Notes</Text>
            </View>

            <View>
                {
                    notes !== undefined ?
                        notes.length > 0 ?
                            notes.map( (item, i) =>
                            (
                         <TouchableOpacity
                             style={styles.item}
                             key={i}
                             onPress={()=>navigation.navigate('Diary', {id:item.id})}
                         >
                        <GestureRecognizer
                                           onSwipeLeft={(gesture)=>onSwipeLeft(gesture,item.id)}
                                           onSwipeRight={(gesture)=>onSwipeRight(gesture,item.id)}
                                           >
                        <Text style={{...globalStyles.h4, color:'#333'}}>{item.text ? item.text: ""}</Text>
                        <Text style={{...globalStyles.h4, color:'#333', textAlign:'right'}}>
                            <MaterialCommunityIcons name='calendar-clock' size={20} />
                            {" "}{moment(item.createdAt).calendar()}
                        </Text>
                        </GestureRecognizer>
                                </TouchableOpacity>
                            )
                            )
                            : <Text>You haven't added any note yet</Text>

                            : <Text>Loading...</Text>
                }
            </View>


        </ScrollView>
            <TouchableOpacity activeOpacity={0.9} onPress={()=>navigation.navigate('Diary')} style={styles.TouchableOpacityStyle} >

                <Image source={require('../../assets/images/add-note.png')}

                       style={styles.FloatingButtonStyle} />

            </TouchableOpacity>

        </View>
    )
};


const styles = StyleSheet.create({
    item: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 9,
        paddingLeft: 9,
        marginBottom:15,
        height:100,
        width:'100%',
        elevation:1,
        borderRadius:5,
        backgroundColor:'#ececec',
        justifyContent: 'space-between'
    },
    TouchableOpacityStyle:{
        position: 'absolute',
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 20,
        zIndex:2000

    },

    FloatingButtonStyle: {
        resizeMode: 'contain',
        width: 30,
        height: 30,
    }
});

DiaryListScreen.navigationOptions = {
    title: 'Diary',
};

export default withNavigation(DiaryListScreen);