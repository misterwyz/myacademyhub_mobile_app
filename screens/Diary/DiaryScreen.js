import React, {useState,useEffect} from "react";
import {View,Text,TouchableOpacity} from "react-native";
import {AsyncStorage,TextInput,StyleSheet} from 'react-native'
const Dimensions = require('Dimensions');
const { width, height } = Dimensions.get('window');
import uuid from 'uuid/v1';
import {Toast} from 'react-native-ui-lib';
import {color} from "react-native-reanimated";
import globalStyles from "../../assets/globalStyles";

const DiaryScreen = (props) =>{

    const [note, setNote] = useState("");
    const [title, setTitle] = useState("Add a note to your dairy");
    const [message, setMessage] = useState("");
    const [toast, setToast] = useState(false);

    useEffect( ()=>{
        EnterScreen();
        LeaveScreen();
    })

    const addNote = async()  =>{
        if(title === "Add a note to your dairy") {
            try {
                if (note === "" || note.length < 5) {
                    setMessage("Note to short")
                    setToast(true);
                    return false;
                }
                const id = uuid();
                const journal_entry = {
                    [id]: {
                        id,
                        text: note,
                        createdAt: Date.now()
                    }
                }


                let diary = await AsyncStorage.getItem('huba_diary');
                if (diary !== null) {
                    diary = JSON.parse(diary);
                    diary.push(journal_entry)
                } else {
                    diary = [];
                }
                await AsyncStorage.setItem('huba_diary', JSON.stringify(diary));
                setMessage("Journal Saved")
                setToast(true);
                setNote("")
            } catch (error) {

                setMessage(error.message)
                setToast(true);
            }
        }else{

                try {
                    const allItems = await AsyncStorage.getItem('huba_diary');

                    if (allItems){
                        let filetred_notes = JSON.parse(allItems).filter(
                            item=> {
                                return  item != null }
                        )

                        filetred_notes = filetred_notes.filter(
                            item=> {
                                return  item != undefined }
                        )

                        filetred_notes = filetred_notes.filter(
                            item=> {
                                let current_obj = Object.keys(item)
                                return item[current_obj].text !== undefined }
                        )

                        filetred_notes.map(
                            item=> {
                                let current_obj = Object.keys(item)
                                if(current_obj[0] === props.navigation.state.params.id){
                                    let current_obj = Object.keys(item)
                                      item[current_obj].text = note
                                      item[current_obj].createdAt = Date.now()
                                }
                            }
                        )


                        await AsyncStorage.setItem('huba_diary', JSON.stringify(filetred_notes));

                        setMessage("Note Updated")
                        setToast(true);

                    }else{
                        setNote("")
                    }

                } catch (err) {
                    setMessage(err.message)
                    setToast(true);
                }
            }
    }

    const EnterScreen = async()=>{

        const removeFocus = props.navigation.addListener('didFocus', async () => {
            if(props.navigation.state.params !== undefined ){
                setTitle("Edit your note");
                try {
                    const allItems = await AsyncStorage.getItem('huba_diary');

                    if (allItems){
                        let filetred_notes = JSON.parse(allItems).filter(
                            item=> {
                                return  item != null }
                        )

                        filetred_notes = filetred_notes.filter(
                            item=> {
                                return  item != undefined }
                        )

                         filetred_notes = filetred_notes.filter(
                            item=> {
                                let current_obj = Object.keys(item)
                                return item[current_obj].text !== undefined }
                        )

                        const transformed_note = filetred_notes.map(
                            item=> {
                                let current_obj = Object.keys(item)
                                 if(current_obj[0] === props.navigation.state.params.id){
                                     let current_obj = Object.keys(item)
                                     return {
                                         id:current_obj[0],
                                         text: item[current_obj].text ,
                                         createdAt: item[current_obj].createdAt
                                     }
                                 }
                            }
                        )



                        setNote(transformed_note[0].text || "")

                    }else{
                        setNote("")
                    }

                } catch (err) {
                    setMessage(err.message)
                    setToast(true);
                }
            }
        });
        return removeFocus;
    }

    const LeaveScreen = ()=>{
        const removeBlur = props.navigation.addListener('willBlur', async () => {
            setTitle("Add a note to your dairy");

        });
        return removeBlur;
    }



    return(
        <View style={{flex:1,flexDirection:"column", padding:15}}>
            <Toast
                visible={toast}
                position={'bottom'}
                backgroundColor={'#FA7921'}
                color={'#fff'}
                message={message}
                onDismiss={() => setToast(false)}
                allowDismiss={true}
                // actions={closeToast}
            />
            <Text style={{...globalStyles.h3Bold,marginBottom:20, marginTop:20}}>{title}</Text>
            <TextInput
                style={styles.input}
                value={note}
                onChangeText={(val)=>setNote(val)}
                placeholder="Type here to add journal for today."
                placeholderTextColor="#333"
                scrollEnabled={true}
                autoFocus={true}

                multiline={true}
                row={8}
                autoCapitalize="sentences"
                underlineColorAndroid="transparent"
                selectionColor={'white'}
                returnKeyType="done"
                autoCorrect={false}
                blurOnSubmit={true}
                // onSubmitEditing={onDoneAddItem}
            />
            <TouchableOpacity onPress={()=>addNote()} style={{
                height:40,
                width:80,
                marginTop:20,
                borderRadius:5,
                alignItems:'center',
                justifyContent: 'center',
                backgroundColor:'#FA7921',
                position: 'absolute',
                right: 30,
                bottom: 30,
            }}>
                <Text style={[globalStyles.h4,{color:'#fff'}]}>Save</Text>
            </TouchableOpacity>
        </View>
    )
};


const styles = StyleSheet.create({
    input: {
        paddingTop: 10,
        paddingLeft: 10,
        fontSize: 15,
        color: '#333',
        fontWeight: '500',
        height:height/3,
        width:width - 60,
        borderLeftWidth:1,
        borderLeftColor:'rgba(0,0,0,0.1)'
    }
});

DiaryScreen.navigationOptions = {
    title: 'Diary',
};

export default DiaryScreen;