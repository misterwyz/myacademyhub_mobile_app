import React, { useState, useEffect} from 'react';
import {Toast} from 'react-native-ui-lib';
import {
    // Image,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    KeyboardAvoidingView,
    Button,
    Image
} from 'react-native';

import {TextBold} from "../../components/TextBold";
import {TextLight} from "../../components/TextLight";
import {httpClientNoAuth} from "../../services/api";
import { Ionicons,EvilIcons } from '@expo/vector-icons';
import Spinner from "../../components/Spinner";
import { connect } from 'react-redux';
import {setUser, processLogin, setToken, authGetToken, logout} from "../../actions/authActions";


const LoginScreen = (props)=> {
    const [username, setUsername] = useState("user6@email.com");
    const [password, setPassword] = useState("Pass@123");
    const [message, setMessage] = useState("");
    const [toast, setToast] = useState(false);

    const login = ()=>{
        setToast(false);
        if(username === ''){
            setMessage("A valid username is required")
            setToast(true);
            return false;
        }

        if(password === ''){
            setMessage("A valid password is required")
            setToast(true);
            return false;
        }
    try {
        props.processLogin(true)
        httpClientNoAuth('auth/login',{email:username, password:password})
            .then( async res=>{

            props.processLogin(false)

            if(res.status !== 'success'){
                    setMessage("Incorrect Username/Password")
                    setToast(true)
            }else{
                if(res.token) {
                    await props.setUser(res)
                    await props.setToken(res.token, res.expires_in)
                    props.navigation.navigate('Main');
                }
            }


        }).catch(e=>{
            props.processLogin(false)
            // console.log(e)
            setMessage(e.message)
            setToast(true)
        })
        // ()=>this.props.navigation.navigate('Main')
    }catch (e) {
        props.processLogin(false)
        props.setUser(undefined)
        // console.log(e.message)
        setMessage("Network Error!. Try again.")
        setToast(true)
    }
    }


    const closeToast = ()=>{
        setToast(false)
    }

    useEffect( ()=>{
        props.processLogin(false);
        // props.logout();
    }, [])

    // console.log(props.auth)
    return (
        <ImageBackground
            source={require('../../assets/images/hub_bg.png')}
            style={{width: '100%', height: '100%'}}
        >
            <Toast
                visible={toast}
                position={'bottom'}
                backgroundColor={'#EB3E3A'}
                message={message}
                onDismiss={() => closeToast()}
                allowDismiss={true}
                // actions={closeToast}
            />
            <View style={styles.container}>

            <View style={{
                // height: height / 2,
                justifyContent:'center',
                alignItems:'center'
            }}>
                <Image source={require('../../assets/images/hub_logo.png')} style={{width:200, height:200}}/>
            </View>

            <KeyboardAvoidingView behavior="padding" style={{
                // height: height / 2,
            }}>
                <TextInput
                    style={styles.input}
                    placeholder={'Phone/Email'}
                    value={username}
                    onChangeText={input=>setUsername(input)}
                    editable={!props.auth.process}
                />

                <TextInput
                    style={styles.input}
                    secureTextEntry={true}
                    placeholder={'Password'}
                    value={password}
                    onChangeText={input=>setPassword(input)}
                    editable={!props.auth.process}
                />


                <TouchableHighlight
                    // disabled={!props.auth.process}
                    style={{...styles.button, backgroundColor:'#FA7921',marginTop:20}}
                    onPress={login}
                >
                    <View style={{ opacity: 1}}>
                        {
                            props.auth.process ?  <Spinner /> : <Text style={{color:'#fff'}}>Sign In</Text>
                        }
                    </View>
                </TouchableHighlight>
            </KeyboardAvoidingView>
                <View

                >
                    <TextLight style={{...styles.h4,textAlign:"center"}}>
                        Forgot Password?
                        {"\n"}
                    </TextLight>

                    <TextLight style={{...styles.h4,textAlign:"center"}}>
                        New User? {" "}
                            <TextBold onPress={()=>props.navigation.navigate('Register')}>Register</TextBold>
                    </TextLight>

                </View>
            </View>

        </ImageBackground>
    )

}



const styles = StyleSheet.create({
    bgImage:{
        flex:1,
        height:null,
        width:null
    },
    button:{
        height:40,
        marginTop:20,
        borderRadius:5,
        alignItems:'center',
        justifyContent: 'center'
    },
    container:{
        flex:1,
        flexDirection:"column",
        paddingTop:60,
        paddingBottom:30,
        paddingLeft:30,
        paddingRight:30,
        justifyContent:"space-between"
    },
    logoContainer:{
        flex:3,
        backgroundColor:'red',
    },
    logo:{
        height:100,
        width:100,
    },
    form:{
        flex:1,
        // justifyContent: 'center',
        backgroundColor:'blue'

    },
    h1:{
        fontSize:40,
        color: '#fff',
        fontWeight: "100"
    },
        h2:{
            fontSize:30,
            color: '#fff',
            fontWeight: "100"
        },
        h3:{
            fontSize:20,
            color: '#fff',
            fontWeight: "100"
        },
        h4:{
            fontSize:15,
            color: '#fff',
            fontWeight: "100"
        },
        h5:{
            fontSize:10,
            color: '#fff',
            fontWeight: "100"
        },
        brandname:{
        fontSize:50,
        color: '#fff'
    },
    input:{
        height:60,
        backgroundColor: 'transparent',
        borderWidth:1,
        borderRadius:5,
        marginBottom:15,
        borderColor:'#ececec',
        paddingLeft:10,
        color:'#fff'
    }
}
);


const mapStateToProps = (state) => {
    const { auth } = state;
    return {auth}
};

export default connect(mapStateToProps, {setUser, processLogin,setToken,authGetToken,logout})(LoginScreen);