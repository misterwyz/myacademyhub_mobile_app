import React, {useState} from 'react';
import {Toast} from 'react-native-ui-lib';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    ImageBackground,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Text
} from 'react-native';

import {TextBold} from "../../components/TextBold";
import {TextLight} from "../../components/TextLight";

import Spinner from "../../components/Spinner";
import { connect } from 'react-redux';
import {setUser, processLogin, setToken, authGetToken, logout} from "../../actions/authActions";
import {httpClientNoAuth} from "../../services/api";

const RegisterScreen = (props)=> {
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [password, setPassword] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [toast, setToast] = useState(false);
    const [message, setMessage] = useState("");

    const register = ()=>{

        setToast(false);
        if(firstname === ''){
            setMessage("A valid First Name is required")
            setToast(true);
            return false;
        }
        if(phone === ''){
            setMessage("A valid phone number is required")
            setToast(true);
            return false;
        }

        if(email === ''){
            setMessage("A valid email is required")
            setToast(true);
            return false;
        }

        if(password === ''){
            setMessage("A valid password is required")
            setToast(true);
            return false;
        }
        try {
            props.processLogin(true)
            httpClientNoAuth('auth/register',{
                email:email,
                password:password,
                firstname:firstname,
                lastname:lastname,
                role: "87",
            })
                .then( async res=>{
                    console.log(res)

                    props.processLogin(false)

                    if(res.status !== 'success'){
                        setMessage(res.message)
                        setToast(true)
                    }else{
                        if(res.token) {
                            await props.setUser(res)
                            await props.setToken(res.token, res.expires_in)
                            props.navigation.navigate('Main');
                        }
                    }


                }).catch(e=>{
                props.processLogin(false)

                setMessage(e.message)
                setToast(true)
            })
            // ()=>this.props.navigation.navigate('Main')
        }catch (e) {
            console.log(e)
            props.processLogin(false)
            props.setUser(undefined)
            // console.log(e.message)
            setMessage("Network Error!. Try again.")
            setToast(true)
        }
    }


    const closeToast = ()=>{
        setToast(false)
    }

        return (
            <ImageBackground
                source={require('../../assets/images/hub_bg.png')}
                style={{width: '100%', height: '100%'}}
            >
                <Toast
                    visible={toast}
                    position={'bottom'}
                    backgroundColor={'#EB3E3A'}
                    message={message}
                    onDismiss={() => closeToast()}
                    allowDismiss={true}
                    // actions={closeToast}
                />
                <ScrollView>
                <View style={styles.container}>
                    <View
                        style={{marginBottom:10}}
                    >
                        <TextLight style={{...styles.h2,color:'#fff'}}>
                            Sign Up
                        </TextLight>

                    </View>
                    <KeyboardAvoidingView behavior="padding" style={{
                        // height: height / 2,
                    }}>
                        <TextInput
                            style={styles.input}
                            placeholder={'First Name'}
                            value={firstname}
                            onChangeText={input=>setFirstname(input)}
                            editable={!props.auth.process}
                        />

                        <TextInput
                            style={styles.input}
                            placeholder={'Last Name'}
                            value={lastname}
                            onChangeText={input=>setLastname(input)}
                            editable={!props.auth.process}
                        />

                        <TextInput
                            style={styles.input}
                            placeholder={'Phone'}
                            value={phone}
                            onChangeText={input=>setPhone(input)}
                            editable={!props.auth.process}
                        />

                        <TextInput
                            style={styles.input}
                            placeholder={'Email'}
                            value={email}
                            onChangeText={input=>setEmail(input)}
                            editable={!props.auth.process}
                        />

                        <TextInput
                            style={styles.input}
                            secureTextEntry={true}
                            placeholder={'Password'}
                            value={password}
                            onChangeText={input=>setPassword(input)}
                            editable={!props.auth.process}
                        />


                        <TouchableOpacity
                            style={{...styles.button, backgroundColor:'#fff',marginTop:20, marginBottom:10}}
                            onPress={register}
                        >
                            <View style={{ opacity: 1}}>
                                {
                                    props.auth.process ?  <Spinner color='#FA7921' /> : <Text style={{color:'#FA7921'}}>Register</Text>
                                }
                            </View>
                        </TouchableOpacity>
                    </KeyboardAvoidingView>
                    <View

                    >
                        <TextLight style={{...styles.h4,textAlign:"center",color: '#fff'}}>
                            Forgot Password?
                            {"\n"}
                        </TextLight>
                        <TextLight style={{...styles.h4,textAlign:"center",color: '#fff'}}>
                            Have an account? {" "}
                            <TextBold onPress={()=>props.navigation.navigate('SignIn')}>Login</TextBold>
                        </TextLight>

                    </View>
                </View>
                </ScrollView>

            </ImageBackground>
        )

}



const styles = StyleSheet.create({
        bgImage:{
            flex:1,
            height:null,
            width:null
        },
        button:{
            height:40,
            marginTop:20,
            borderRadius:35,
            alignItems:'center',
            justifyContent: 'center'
        },
        container:{
            flex:1,
            flexDirection:"column",
            paddingTop:60,
            paddingBottom:30,
            paddingLeft:30,
            paddingRight:30,
            justifyContent:"space-between"
        },
        logoContainer:{
            flex:3,
            backgroundColor:'red',
        },
        logo:{
            height:100,
            width:100,
        },
        form:{
            flex:1,
            // justifyContent: 'center',
            backgroundColor:'blue'

        },
        h1:{
            fontSize:40,
            fontWeight: "100"
        },
        h2:{
            fontSize:30,
            fontWeight: "100"
        },
        h3:{
            fontSize:20,
            fontWeight: "100"
        },
        h4:{
            fontSize:15,
            fontWeight: "100"
        },
        h5:{
            fontSize:10,
            fontWeight: "100"
        },
        brandname:{
            fontSize:50,
            color: '#fff'
        },
        input:{
            height:60,
            backgroundColor: 'transparent',
            borderWidth:1,
            borderRadius:5,
            marginBottom:15,
            borderColor:'#ececec',
            paddingLeft:10,
            color:'#fff'
        }
    }
);


const mapStateToProps = (state) => {
    const { auth } = state;
    return {auth}
};

export default connect(mapStateToProps, {setUser, processLogin,setToken,authGetToken})(RegisterScreen);