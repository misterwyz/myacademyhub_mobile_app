import React, { Component } from 'react';
import { Animated, Easing } from 'react-native';
import  { EvilIcons } from  '@expo/vector-icons';

class Spinner extends Component {

    constructor(props){
        super(props)
    }
    spinValue = new Animated.Value(0);

    componentDidMount(){
        this.spin();
    };

    spin = () => {

        this.spinValue.setValue(0);

        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: true
            }
        ).start(() => this.spin());

    };

    render() {

        const { style, children } = this.props;
        const rotate = this.spinValue.interpolate({inputRange: [0, 1], outputRange: ['0deg', '360deg']});

        return(
            <Animated.View style={{transform: [{rotate}]}}>
                <EvilIcons name="spinner-3" style={style} size={32} color={this.props.color ? this.props.color : "#fff"}>
                    {children}
                </EvilIcons>
            </Animated.View>
        )

    }
}

export default Spinner;