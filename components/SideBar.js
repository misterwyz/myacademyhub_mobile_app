import React, {useEffect} from "react";
import {View,ScrollView, StyleSheet, TouchableOpacity, Text} from "react-native";
import globalStyles from "../assets/globalStyles";
import {AntDesign} from "@expo/vector-icons";



const SideBar = ({name, navigator,logout})=>{

    return(
        <ScrollView style={styles.container}>
        <View style={styles.profileContainer}>
            <AntDesign name={'user'} size={40} color="#2D2B4A" />
            <TouchableOpacity>
            <Text style={{...globalStyles.h3,fontFamily: 'gilroy-light', marginTop:10}}>Profile</Text>
            </TouchableOpacity>
        </View>

            <View style={{height:1, backgroundColor:'rgba(0,0,0,0.2)', marginBottom:10}}/>

            <View style={styles.menuContainer}>
            <TouchableOpacity>
                <Text style={{...globalStyles.h4, padding:10}}>HOME</Text>
            </TouchableOpacity>
                <TouchableOpacity>
                <Text style={{...globalStyles.h4,padding:10}}>SUBSCRIPTIONS</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                <Text style={{...globalStyles.h4,padding:10}}>MESSAGES</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>navigator.navigate('Reflections')}>
                <Text style={{...globalStyles.h4,padding:10}}>REFLECTIONS</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                <Text style={{...globalStyles.h4,padding:10}}>TASKS</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button}>
                    <Text style={{...globalStyles.h5,padding:10, color:'#fff'}} onPress={()=> {
                        logout()
                    }
                    }>LOGOUT</Text>
                </TouchableOpacity>

            </View>
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        // marginRight:60,
        color:'#fff',
        backgroundColor:'#fff',
        zIndex: 5
    },
    button:{
        height:40,
        marginTop:20,
        borderRadius:25,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor: '#FA7921',
        paddingLeft:20,
        paddingRight:20,
    },
    profileContainer:{
        flex:2,
        height:150,
        justifyContent:'center',
        alignItems:'flex-start',
        paddingLeft: 15
    },
    menuContainer:{
        flex:4,
        marginTop:15,
        alignItems:'flex-start',
        paddingLeft: 15
    }
})

export default SideBar;