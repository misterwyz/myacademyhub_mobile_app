import React, {useState,useEffect} from 'react';
import {EvilIcons, MaterialCommunityIcons} from "@expo/vector-icons";
import {
    View,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    ScrollView
} from 'react-native';
const Dimensions = require('Dimensions');
import globalStyles from "../assets/globalStyles";
import {TextBold} from "./TextBold";
const { width, height } = Dimensions.get('window');
const wallet = require('../assets/images/walletp.png');
const TopUpOverlayAlert = (props)=>{
    return(

        <View style={[styles.overlay,
            { height: props.show ? height : 0}
        ]}>
            <View style={[styles.overlayHigher,{
                height: height/1.3,
                backgroundColor: '#f4f4f4',
                justifyContent:'center',
                alignItems:'center',
                flexDirection:'column',
                position: "absolute",
                bottom: 0
            }]} >

                <View style={[styles.overlayHigher,{
                    flex:1,
                    justifyContent:'space-between',
                    alignItems:'center',
                    flexDirection:'row',
                    paddingLeft: 20,
                    paddingRight: 20,
                }]}>
                    <Text>Choose a Payment method to continue</Text>
                    <EvilIcons name={'close'} size={24} onPress={()=>props.close()}/>
                </View>
                <View style={[styles.overlayHigher,{
                    flex:4,
                    backgroundColor:'#fff',
                    padding: 20,
                    justifyContent:'center',
                    flexDirection:'row'
                }]} >


                    <View style={{ borderWidth:3,
                        borderColor:'rgba(158, 150, 150, .1)',
                        borderRadius:5,
                        width:'100%',
                        height:80,
                        justifyContent:'space-between',
                        alignItems:'center',
                        flexDirection:"row",
                        paddingLeft:15,
                        paddingRight:15,
                    }}>
                        <View
                            style={{
                                justifyContent:'space-between',
                                alignItems:'center',
                                flexDirection:"row",
                            }}
                        >
                            <Image source={wallet} style={{width:50, height:50, marginRight:10}}/>
                            <TextBold style={{...styles.h3, ...styles.textDark}}>Wallet</TextBold>
                        </View>
                        <View
                            style={{
                                justifyContent:'space-between',
                                alignItems:'center',
                                flexDirection:"column",
                            }}
                        >
                            <Text style={{...styles.h3, ...styles.textDark}}>
                                <MaterialCommunityIcons name={'currency-ngn'} size={16} color="#333"/>
                                <Text style={{ textDecorationLine: 'line-through'}}>8000</Text>
                            </Text>
                            <Text style={{...styles.h3, ...styles.textDark}}>
                                <MaterialCommunityIcons name={'currency-ngn'} size={18} color="#333"/>8000
                            </Text>
                        </View>
                    </View>

                </View>
                <View style={{...styles.overlayHigher,...globalStyles.bg1, elevation: 2,flex:2,
                }}>
                    <TouchableOpacity
                        style={{
                            height:30,
                            padding:25,
                            borderRadius:5,
                            alignItems:'center',
                            justifyContent:'center',
                            flexDirection:"row"
                        }}
                        onPress={()=>proceed()}
                    >
                        <Text style={{...globalStyles.textWhite, ...globalStyles.h3}}>Pay</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    ...globalStyles,

    overlay: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
        opacity: 0.8,
        backgroundColor: 'black',
        width: width,
        justifyContent:"center",
        alignItems:"center",
        zIndex:1000
    },
    overlayHigher: {
        // flex: 1,
        // ...StyleSheet.absoluteFillObject,
        width: width,
        zIndex:2000
    },
    bottomContainer: {
        justifyContent:'flex-end',
        flexDirection: 'column',
        marginTop:35
    },
});

export default TopUpOverlayAlert;