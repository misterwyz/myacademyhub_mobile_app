import { combineReducers } from 'redux';
import authReducer from './authReducer';
import reflectionReducer from './reflectionReducer';
import contentReducer from './contentReducer';

export default combineReducers({
    auth: authReducer,
    reflection: reflectionReducer,
    content: contentReducer,
});