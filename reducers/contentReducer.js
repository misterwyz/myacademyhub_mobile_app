import { CONTENT_CATEGORY} from '../types';

const INITIAL_STATE = {
    process:false,
    categories:undefined
};

const contentReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CONTENT_CATEGORY:
            return {
                ...state,
                categories: action.payload
            }
        default:
            return state
    }
};

export default contentReducer;