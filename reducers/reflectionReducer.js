import { REFLECTIONS,REFLECTION_FOR_TODAY} from '../types';

const INITIAL_STATE = {
    process:false,
    reflections:undefined,
    reflection_for_today:undefined,
};

const reflectionReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case REFLECTIONS:
            return {
                ...state,
                reflections: action.payload
            }
            case REFLECTION_FOR_TODAY:
            return {
                ...state,
                reflection_for_today: action.payload
            }
        default:
            return state
    }
};

export default reflectionReducer;