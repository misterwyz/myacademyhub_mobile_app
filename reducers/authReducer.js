import { PROCESS_LOGIN, USER, SET_TOKEN,LOGOUT} from '../types';

const INITIAL_STATE = {
    process:false,
    user:undefined,
    token:undefined,
    expires_in:undefined,
};

const authReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case USER:
            return {
                ...state,
                user: action.payload
            }
        case SET_TOKEN:
            return {
                ...state,
                token: action.payload.token,
                expires_in: action.payload.expires_in,
            }
        case PROCESS_LOGIN:
            return {
                ...state,
                process: action.payload
            }
        case LOGOUT:
            return {
                ...state,
                user: false,
                token: false,
                expires_in: false,
                process: false,
            }
        default:
            return state
    }
};

export default authReducer;